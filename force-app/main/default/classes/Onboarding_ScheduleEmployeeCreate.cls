/**
* @name         Onboarding_ScheduleEmployeeCreate 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  This schedulable class executes Onboarding_BatchCreateEmployeeRec .
* @testClass    
**/

public with sharing class Onboarding_ScheduleEmployeeCreate implements Schedulable {
    
    public void execute(SchedulableContext sc) {

        Onboarding_BatchCreateEmployeeRec obj = new Onboarding_BatchCreateEmployeeRec();
        Id batchId = Database.executeBatch(obj);
        
    }
}