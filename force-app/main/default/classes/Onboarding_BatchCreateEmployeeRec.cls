/**
* @name         Onboarding_BatchCreateEmployeeRec 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  This batch class creates employee(Contact) records for hired candidates whose 
*               joining date is tomorrow and contact field is null.
* @testClass    Onboarding_BatchCreateEmployeeRecTest
**/

public inherited sharing class Onboarding_BatchCreateEmployeeRec implements Database.Batchable<SObject>, Database.stateful{
    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('im in start of batch');
        Date tomorrow = Date.today().addDays(1); 
        string query =  'SELECT  Name, Email__c, Date_Of_Joining__c, Phone_No__c, Work_Location__c, ' +
                        'Job_Role__c, First_Name__c, Last_Name__c FROM Candidate__c WHERE Status__c = \'Hired\' ' +
                        'AND Date_Of_Joining__c = :tomorrow AND Contact__c = NULL ' + 
                        'WITH SECURITY_ENFORCED LIMIT 5000';
        System.debug(query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Candidate__c> records) {
        
        System.debug('inside batch execute');    
        RecordType indiaEmployeeRecordType =   [SELECT ID 
                                                FROM RecordType
                                                WHERE DeveloperName = 'India_Employee'
                                                WITH SECURITY_ENFORCED
                                                LIMIT 1];
        RecordType australiaEmployeeRecordType =   [SELECT ID 
                                                    FROM RecordType
                                                    WHERE DeveloperName = 'Australia_Employee'
                                                    WITH SECURITY_ENFORCED
                                                    LIMIT 1]; 

        Account cloudwerxAccount = [SELECT ID 
                                    FROM Account
                                    WHERE Name = 'Cloudwerx'
                                    WITH SECURITY_ENFORCED
                                    LIMIT 1];                   //? 
                                
        System.debug('records received :' + records);                        
        List<Contact> lstOfContactsToInsert = new List<Contact>();
        for (Candidate__c objCandidate : records) {
            Contact newContact = new Contact();
            switch on objCandidate.Work_Location__c {
                when 'Pune Office' {
                    newContact.RecordTypeId = indiaEmployeeRecordType.Id;
                }
                when 'Sydney Office' {
                    newContact.RecordTypeId = australiaEmployeeRecordType.Id;
                }
            }
            
            newContact.FirstName = objCandidate.First_Name__c;
            newContact.LastName = objCandidate.Last_Name__c;
            newContact.Email = objCandidate.Email__c;
            newContact.Date_Of_Joining__c = objCandidate.Date_Of_Joining__c;
            newContact.Phone = objCandidate.Phone_No__c;
            newContact.AccountId = cloudwerxAccount.Id;
            newContact.Work_Location__c = objCandidate.Work_Location__c;
            newContact.Employee_Role__c = objCandidate.Job_Role__c;
            newContact.Employee_Type__c = 'Employee';
            newcontact.Status__c = 'Accepted Offer';
            lstOfContactsToInsert.add(newContact);
        }

        System.debug('lstOfContactsToInsert : ' + lstOfContactsToInsert);
        try {
            Database.SaveResult[] srList = Database.insert(lstOfContactsToInsert, false); 
            System.debug('save result: ' + srList);
            for (Contact objContact : lstOfContactsToInsert) {
                System.debug('current contact : ' + objContact.FirstName);
                System.debug('current contact Id : ' + objContact.Id);
                for (Database.SaveResult sr : srList) {
                    System.debug('current sr : ' + sr);
                    if (objContact.Id == sr.getId()) {
                        System.debug('indide second if objContact.Id == sr.getId()');
                        for (candidate__C objCandidate : records) {
                            System.debug('current cadidate  : ' + objCandidate.First_Name__c);
                            if (objCandidate.Email__c == objContact.Email) {
                                System.debug('inside if objCandidate.Email__c == objContact.Email');
                                //if contact is created successfully update it on candidate.
                                objCandidate.Contact__c = objContact.Id;  
                                System.debug('contact obj : ' + objCandidate.Contact__c);
                            }
                        }
                    }
                }
                
                
            }
            update records;
        
        } catch (Exception e) {
            System.debug('Error while inserting the contact'+ e.getMessage());
        }
    }

    public void finish(Database.BatchableContext bc) {

    }
}