/**
* Description   : Controller for Cloudwerx_Certifications Lightning Component
* Author		: Cloudwerx
* Created Date	: 09 December 2020
* Code Coverage : CloudwerxCertificationsController_Test
* */ 
public Without Sharing class CloudwerxCertificationsController {
    
    /**
    * Description : Method queries the existing and new certifications based on the email address. 
    * Params	   : Contact Email Address
    * Return	   : ActionResult with List old and new Certifications	
    **/
    @AuraEnabled
    public static ActionResult getCertifications(String userId){
        System.debug('userId is : ' + userId);
        Map<String, CertificationDependency.MyWrapper> constantCertificationValues = new Map<String, CertificationDependency.MyWrapper>();
        String ErrorMessage = 'Invalid Email Address. Please Check Email Address!';
        Map<String, Object> mapOfCertificationData = new Map<String, Object>();
        List<wrapcertification> remainingCertificationList = new List<wrapcertification>();
        
        ActionResult result = new ActionResult();
        List<Contact> cntList = new List<Contact>();
        cntList = [SELECT Id FROM Contact WHERE Id = :userId LIMIT 1];
        
        List<Certification__c> existingCertsList = new List<Certification__c>();
        existingCertsList = [SELECT ID,Certification_Name__c,Date_Achieved__c,Contact__r.Name
                             FROM Certification__c
                             WHERE Contact__c = :userId ORDER BY Certification_Name__c ASC];
        
        List<String> existingCertifications = new List<String>();
        
        if(cntList.size() > 0){
            if(!existingCertsList.isEmpty()){
                constantCertificationValues = CertificationDependency.certDependencyMap();
                for(Certification__c certs: existingCertsList){
                    existingCertifications.add(certs.Certification_Name__c);
                }
                for(SelectOption so : getCertifications()){
                    if(!existingCertifications.contains(String.valueOf(so.getLabel())) && cntList.size() > 0){
                        Certification__c cert = new Certification__c(Certification_Name__c = String.valueOf(so.getLabel()),
                                                                     Date_Achieved__c= System.TODAY(),Contact__c = cntList[0].Id);
                        
                    	//Check if constant map has certification name
                        if(constantCertificationValues.containsKey(String.valueOf(so.getLabel())))
                        {
                            CertificationDependency.MyWrapper mywrap = constantCertificationValues.get(String.valueOf(so.getLabel()));
                            cert.Specialization_Certification__c = mywrap.specializationCertification;
                            cert.Specialization_Category__c = mywrap.specializationCategory;
                            cert.Relevant_Specialization__c =  string.join(mywrap.relevantSpecializationList,';');
                        }
                        remainingCertificationList.add(new wrapcertification(cert));
                    }
                }
            }
            else
            {
                constantCertificationValues = CertificationDependency.certDependencyMap();
                for(SelectOption so : getCertifications()){
                    if(cntList.size() > 0){
                        Certification__c cert = new Certification__c(Certification_Name__c = String.valueOf(so.getLabel()),
                                                                     Date_Achieved__c= System.TODAY(),Contact__c = cntList[0].Id);
                        
                        //Check if constant map has certification name
                        if(constantCertificationValues.containsKey(String.valueOf(so.getLabel())))
                        {
                            CertificationDependency.MyWrapper mywrap = constantCertificationValues.get(String.valueOf(so.getLabel()));
                            cert.Specialization_Certification__c = mywrap.specializationCertification;
                            cert.Specialization_Category__c = mywrap.specializationCategory;
                            cert.Relevant_Specialization__c =  string.join(mywrap.relevantSpecializationList,';');
                        }
                        remainingCertificationList.add(new wrapcertification(cert));
                    }
                }
            }
        }
        if(cntList.size() <= 0){
         	ErrorMessage  = 'Invalid Email Address. Please Check Email Address!';
        }
        else if(existingCertsList.size() <= 0){
            ErrorMessage = 'No Existing Certification Found!. Please Check!';
        }
        else
        {
            ErrorMessage = 'No Remaining Certification Found!. Please Check!';
        }
        mapOfCertificationData.put('existingCerts', existingCertsList);
        mapOfCertificationData.put('remainingCerts', remainingCertificationList);
        mapOfCertificationData.put('ErrorMessage', ErrorMessage);
        result.setSuccess(mapOfCertificationData);
        return result;
    }
    
    /**
    * Description : Method add new Certification 
    * Params	   : 1. List of all the Remaining Certification  2. Contact Email Address
    * Return	   : ActionResult with List old and new Certifications	
    **/
    @AuraEnabled
    public static ActionResult insertNewCertifications(List<wrapcertification> certificationList, String userId){
        ActionResult result = new ActionResult();
        System.debug('certificationList : ' + certificationList);
        System.debug('userId : ' + userId);
        try{
        List<Certification__c> newCertificationList = new List<Certification__c>();
        for(wrapcertification wc : certificationList){
            if(wc.isselected){
                newCertificationList.add(wc.certification);
            }
        }
       
        
        	if(!newCertificationList.isEmpty()){
            	insert newCertificationList;
            	result = getCertifications(userId);
        	}
        }
        Catch(Exception e){
            System.debug('error : ' + e.getMessage());
            System.debug('error line number : ' + e.getLineNumber());
            result.setFailure(e);
        }
        return result; 
    }
    
    /**
    * Description  : Method retrives all the picklist values in Certification Name Field in Certification Object. 
    * Params	   : None
    * Return	   : List of all Certifications	
    **/
    public static List<SelectOption> getCertifications()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Certification__c.Certification_Name__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    //Wrapper Class for Certification
    public class wrapcertification{
        @AuraEnabled public Certification__c certification{get;set;}
        @AuraEnabled public boolean isSelected{get;set;}
        
        public wrapcertification(){
            //Do Nothing
        }
        public wrapcertification(Certification__c c){
            certification=c;
            isselected=false;
        }
    }
    
}