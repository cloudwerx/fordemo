/**
* @name         Onboarding_SlackResponseMock 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  MockResponse class for slack callouts for testing
* @testClass    Onboarding_SlackReminderTest
**/
@isTest
public inherited sharing class Onboarding_SlackResponseMock implements HttpCalloutMock{
    // string method;
    public Onboarding_SlackResponseMock() {
        // this.method = method;
    }

    public HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-type', 'application/json');
        res.setStatusCode(200);

        if(req.getEndpoint().contains('conversations.invite')){
            res.setStatus('OK');
            res.setBody(addToChannelSucResponse());    
        } else if (req.getEndpoint().contains('users.list')) {
            res.setStatus('OK');
            res.setBody(userListSucResponse());    
        } else if(req.getEndpoint().contains('chat.postMessage')) {
            res.setStatus('OK');
            res.setBody(postMessageResponse());
        }
        return res;
    }

    private string addToChannelSucResponse() {
        String body = '{\"ok\":true,\"channel\":{\"id\":\"G01JTC9PJAX\",\"name\":\"sf-slack-integration-private\",\"is_channel\":false,\"is_group\":true,\"is_im\":false,\"created\":1610685914,\"is_archived\":false,\"is_general\":false,\"unlinked\":0,\"name_normalized\":\"sf-slack-integration-private\",\"is_shared\":false,\"parent_conversation\":null,\"creator\":\"U01E57W4MUZ\",\"is_ext_shared\":false,\"is_org_shared\":false,\"shared_team_ids\":[\"T9LF4SCR0\"],\"pending_shared\":[],\"pending_connected_team_ids\":[],\"is_pending_ext_shared\":false,\"is_member\":true,\"is_private\":true,\"is_mpim\":false,\"last_read\":\"1613370584.000100\",\"is_open\":true,\"topic\":{\"value\":\"\",\"creator\":\"\",\"last_set\":0},\"purpose\":{\"value\":\"This Channel is to test integration with slack for onboarding app\",\"creator\":\"U01E57W4MUZ\",\"last_set\":1610685915}},\"warning\":\"missing_charset\",\"response_metadata\":{\"warnings\":[\"missing_charset\"]}}';
        return body;
    }

    private string userListSucResponse() {
        string body = '{' + 
            '"ok": true,' + 
            ' "members": [{'+ 
            ' "id": "U01E57W4MUZ",'+ 
            ' "team_id": "T9LF4SCR0",'+ 
            ' "name": "gbaviskar",'+ 
            ' "deleted": false,'+ 
            '"real_name": "Girish Baviskar",'+ 
            '"profile": {'+ 
            '"title": "Salesforce Developer Trainee",'+ 
            '"phone": "",'+ 
            ' "skype": "",'+ 
            '"real_name": "Girish Baviskar",'+ 
            ' "real_name_normalized": "Girish Baviskar",'+ 
            ' "display_name": "Girish",'+ 
            '"display_name_normalized": "Girish",'+ 
            ' "email": "test0@onboarding.com",'+ 
            ' "image_512": "https://avatars.slack-edge.com/2020-11-02/1475796298676_da8d1fd6e8f6f47e49e4_512.png"'+ 
           ' }'+
           '}]'+
           '}';
        return body;
    }
    
    private string postMessageResponse() {
        string body = '{"ok": true}';
        return body;
    }
}