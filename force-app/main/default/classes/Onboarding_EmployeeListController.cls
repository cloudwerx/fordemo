/**
* @name         Onboarding_EmployeeListController  
* @author       Girish Baviskar
* @date         20-01-2021
* @description  controller for EmployeeList LWC.
* @testClass    Onboarding_EmployeeListControllerTest
**/ 
public with sharing class Onboarding_EmployeeListController {
    
    /**
    * @description This methods returns list of all the employees who are going through onboarding 
    *              process and only if requesting person has employer status.
    * @param       requestingContactId contact id of person who is requesting the onboarding list.
    * @return     `List<Contact>`
    **/
    @AuraEnabled
    public static List<Contact> getCurrentOnboardingEmployees(Id requestingContactId){
        System.debug('requestingContactId : ' + requestingContactId);
        try {

            /* RecordType indiaEmployeeRecordType =   [SELECT ID 
                                                FROM RecordType
                                                WHERE DeveloperName = 'India_Employee'
                                                WITH SECURITY_ENFORCED
                                                LIMIT 1];
            RecordType australiaEmployeeRecordType =   [SELECT ID 
                                                    FROM RecordType
                                                    WHERE DeveloperName = 'Australia_Employee'
                                                    WITH SECURITY_ENFORCED
                                                    LIMIT 1];  */
            Contact requestingContact = [   SELECT  Work_Location__c,
                                                    Employee_Type__c 
                                            FROM Contact    
                                            WHERE id =: requestingContactId
                                            WITH SECURITY_ENFORCED
                                            LIMIT 1];
            if (requestingContact != null && requestingContact.Employee_Type__c == 'Employer') {
                List<Contact> lstContact =  [SELECT  Name,
                                                        Id,
                                                        Employee_Role__c,
                                                        Date_Of_Joining__c,
                                                        Work_Location__c
                                            FROM Contact
                                            WHERE (Is_Employee_Onboarding_Complete__c = false
                                            OR  Is_Employer_Onboarding_Complete__c = false)
                                            AND Work_Location__c = :requestingContact.Work_Location__c
                                            AND Date_Of_Joining__c != null
                                            WITH SECURITY_ENFORCED
                                            LIMIT 10000];
                if (lstContact.size() > 0) {
                    return lstContact;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            System.debug('error' + e.getMessage());
            return null;
        }
    }
}