({
	doInit : function(component, event, helper) {
		//Do Something	
        var contactId = new URL(window.location.href).searchParams.get('recordId');
        component.set("v.recordId", contactId);
        //console.log(contactId);
        helper.fetchCertificationsHelper(component, event, helper);
	},
    
   /*  fetchCertifications : function(component, event, helper) {
    	
	}, */
    
    addNewCertifications : function(component, event, helper){
        //console.log('Controller Called=====>');
        helper.newCertificationHelper(component, event, helper);
    },
	
	// function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }    
})