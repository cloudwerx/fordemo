/************************************************************************************************
* @Name         Onboarding_ContactTrigger 
* @Author       Girish Baviskar
* @Date         15-01-2021
* @Description  Trigger on contact object. 
* @TestClass  
* @HandlerClass ContactTriggerHandler  
*************************************************************************************************/

/* MODIFICATION LOG
* Version   Developer           Date         Ticket Number          Description
*------------------------------------------------------------------------------------------------
*  1        Yash Bhalerao       28.01.2021                          Added after insert for task creation   
*/


trigger Onboarding_ContactTrigger on Contact (After update, after insert) 
{
    if (Trigger.isAfter && Trigger.isUpdate) {
        Onboarding_ContactTriggerHandler.handlerAfterUpdate(Trigger.new, Trigger.oldMap);  
    }
    if(Trigger.isAfter && Trigger.isInsert)
    {
        Onboarding_ContactTriggerHandler.handlerAfterInsert(Trigger.new);
    }
}