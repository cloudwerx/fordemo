/**
* @name         Onboarding_EmployerTaskView
* @author       Girish Baviskar
* @date         15-1-2021
* @description  This cmp shows list of employer tasks for employee.
**/
import { LightningElement, track, api, wire } from 'lwc';
import getEmployerTaskDetails from '@salesforce/apex/Onboarding_EmployerTaskViewController.getEmployerTaskDetails';
import updateTaskStatus from '@salesforce/apex/OnboardingUIController.updateTaskStatus';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import insertTask from '@salesforce/apex/Onboarding_EmployerTaskViewController.insertTask';

const columns = [
    { label : 'Subject', fieldName : 'Subject', editable: false, wrapText: true, hideDefaultActions: true},
    { label : 'Description', fieldName : 'Description', editable: false, wrapText: true, intialWidth : 40, hideDefaultActions: true},
    { label: 'Status', fieldName: 'Is_Completed__c', type: 'toggleButton', initialWidth: 80, editable: false, hideDefaultActions: true,
            typeAttributes: { 
                buttonDisabled: { fieldName: 'isDisabled' }, 
                rowId: { fieldName: 'Id' }, 
            }
        }
];
export default class Onboarding_EmployerTaskView extends LightningElement 
{
    
    @api contactId; // contact id for which employer tasks are being shown.
    
    @track contactName;
    @track taskData;
    @track data; // table data

    @track showSendOtp = true;
    @track showLogin = false;
    columns = columns;

    @api isonboardingcomplete;
    @api usertype;

    @track isTaskModalOpen = false;

    @track task = {'sObjectType': 'Task'};

    @track showLoadingSpinner = false;

    @track showDatatable;
    @track hideDatatable;

    get options() {
        return [
            { label: 'Employee', value: 'Employee' },
            { label: 'Employer', value: 'Employer' }
        ];
    }

    @wire(getEmployerTaskDetails,{contactId : '$contactId'})
    dataList;
    
    connectedCallback() 
    {
        this.showLoadingSpinner = true;
        // console.log('inside connected callback');
        // console.log('contact id received' + this.contactId);
        this.getEmployerTasks();
    }

    renderedCallback()
    {
        //this.showDatatable = true;
        //this.hideDatatable = false;

        //console.log('this.dataList.data : ' + this.dataList.data);
        //this.data = this.dataList.data;
        //console.log('this.data : ' + this.data);

        // console.log('this.usertype : ' + this.usertype);
        if(this.usertype != 'Employer')
        {
            this.disableCmp();
        }
    }

    disableCmp()
    {
        var element = this.template.querySelectorAll(".employer").forEach(y=>{
                //console.log('y : ' + y);
                y.className="approved";
             });

        var element1 = this.template.querySelectorAll(".hideButton").forEach(z=>{
                z.className="slds-hide";
             });
    }

    //method to show toast.
    showToast(toastTitle, toastMessage, toastVarient) {
        const evt = new ShowToastEvent({
            title: toastTitle,
            message : toastMessage,
            variant: toastVarient,
        });
        this.dispatchEvent(evt);
    }


    //gets all employer tasks for current contact
    getEmployerTasks() 
    {
        // console.log('inside getEmployerTasks()');
        getEmployerTaskDetails({contactId : this.contactId}).then(res =>{
            //console.log(res.length);
            if(res == null)
            {
                this.showDatatable = false;
                this.hideDatatable = true;
            }
            else
            {
                this.showDatatable = true;
                this.hideDatatable = false;
            }
            this.data = res;
            this.showLoadingSpinner = false;
            // console.log('Result' + res);
        }).catch(err => console.log(err));
    }
    handleSelectedRec(event) 
    {
        // console.log('event.detail.value.rowId : ' + event.detail.value.rowId);

        updateTaskStatus({taskToUpdateId : event.detail.value.rowId}).then(response =>{
            //refreshApex(this.data);
            this.showToast('Status of task changed successfully.','','success');
            }).catch(error => {
            // console.log('Error - Cannot change status: ' + error);
            this.showToast('Unexpected Error Occured','Sorry for trouble but please try again','error');
            });
    }
    
    openTaskModal() {
        // to open modal set isModalOpen tarck value as true
        this.isTaskModalOpen = true;
    }
    closeTaskModal() {
        // to close modal set isModalOpen tarck value as false
        this.isTaskModalOpen = false;
    }

    handleInputChange(event)
    {
        if(event.target.name == 'taskType')
        {
            //console.log('event.detail.value : ' + event.detail.value);
            this.task.Task_Type__c = event.detail.value;
        }
        else if(event.target.name == 'subject')
        {
            this.task.Subject = event.target.value;
        }
        else if(event.target.name == 'description')
        {
            this.task.Description = event.target.value;
        }
        else if(event.target.name == 'upload')
        {
            //console.log('event.target.checked : ' + event.target.checked);
            this.task.Is_Upload_Required__c = event.target.checked;
        }
        else
        {
            this.task.Employer_Comments__c = event.target.value;
        }
    }

    async createNewTask()
    {
        if(this.task.Subject == null || this.task.Description == null)
        {
            this.showToast('Please enter all required fields.','','info');
        } else {
            this.isTaskModalOpen = false;
            this.showLoadingSpinner = true;

            //console.log('this.task : ' + this.task);
            await insertTask({taskToInsert : this.task, contactId : this.contactId}).then(res => {
                this.showLoadingSpinner = false;
                this.showToast('Task added successfully.','','success');
            }).catch(err => {
                this.showToast('Something went wrong while adding task .', err,'info');
                //console.error(err);
            });
    
            
            this.getEmployerTasks();;
        }

        //this.connectedCallback();
        //this.renderedCallback();
        //location.reload();
    }
}